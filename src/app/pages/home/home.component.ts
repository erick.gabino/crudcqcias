import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PersonaModel } from 'src/app/models/persona-model';
import { HomeService } from "src/app/service/home.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit{
  listaPersonas: PersonaModel [] = [];
  formPersonas: FormGroup = new FormGroup({});
  esActualizar: boolean = false;
  esVisualizar:boolean = false;

  constructor(private homeService: HomeService){}

  ngOnInit(): void{
    this.list();
    this.formPersonas = new FormGroup({
      id:new FormControl(''),
      nombre:new FormControl(''),
      primer_apellido:new FormControl(''),
      segundo_apellido:new FormControl(''),
      telefono:new FormControl(''),
      estatus:new FormControl(''),
      fechaIns:new FormControl(''),
      fechaUpd:new FormControl('')
    });
  }

  list(){
    this.homeService.getPersonas().subscribe(resp=>{
      if(resp){
        this.listaPersonas = resp;
      }
    });
  }

  nuevaPersona(){
    this.esActualizar=false;
    this.esVisualizar = false;
    this.formPersonas.reset();
  }

  visualizarPersona(persona: any){
    this.esVisualizar=true;
    this.esActualizar=false;
    this.obtenerPersona(persona);
  }

  seleccionarPersona(persona: any){
    this.esActualizar=true;
    this.esVisualizar = false;
    this.obtenerPersona(persona);
  }

  agregarUsuario(){
    this.homeService.savePersonas(this.formPersonas.value).subscribe(resp=>{
      if(resp){
        this.list();
        this.formPersonas.reset();
      }
    });
  }

  editarPersona(){
    this.homeService.updatePersonas(this.formPersonas.value).subscribe(res=>{
      if(res){
        this.list();
        this.formPersonas.reset();
      }
    });
  }

  borrarPersona(id:any){
    this.homeService.deletePersonas(id).subscribe(resp=>{
      if(resp){
        this.list();
      }
    });
  }

  obtenerPersona(persona: any){
    this.formPersonas.controls['id'].setValue(persona.id);
    this.formPersonas.controls['nombre'].setValue(persona.nombre);
    this.formPersonas.controls['primer_apellido'].setValue(persona.primer_apellido);
    this.formPersonas.controls['segundo_apellido'].setValue(persona.segundo_apellido);
    this.formPersonas.controls['telefono'].setValue(persona.telefono);
    this.formPersonas.controls['estatus'].setValue(persona.estatus);
    this.formPersonas.controls['fechaIns'].setValue(new Date(persona.fecha_ins));
    this.formPersonas.controls['fechaUpd'].setValue(new Date(persona.upd));
  }
}
