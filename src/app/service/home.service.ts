import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { PersonaModel } from '../models/persona-model';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpClient: HttpClient) { }

  getPersonas(): Observable<PersonaModel[]>{
    return this.httpClient.get<PersonaModel[]>('http://localhost:1550/api/cqcias'+'/list').pipe(map(res => res));
  }
  savePersonas(request:any): Observable<any>{
    return this.httpClient.post<any>('http://localhost:1550/api/cqcias'+'/save', request).pipe(map(res => res));
  }
  updatePersonas(request: any): Observable<any>{
    return this.httpClient.post<any>('http://localhost:1550/api/cqcias'+'/update', request).pipe(map(res => res));
  }
  deletePersonas(id: number): Observable<any>{
    return this.httpClient.get<any>('http://localhost:1550/api/cqcias'+'/delete/'+id).pipe(map(res => res));
  }
}
